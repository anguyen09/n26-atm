FlowRouter.route( '/', {
  action: function() {
    BlazeLayout.render( 'applicationLayout', { main: 'welcome' } ); 
  },
  name: 'welcome'
});


FlowRouter.route( '/welcome', {
  action: function() {
    BlazeLayout.render( 'applicationLayout', { main: 'welcome' } ); 
  },
  name: 'welcome'
});


FlowRouter.route( '/pin', {
  action: function() {
    BlazeLayout.render( 'applicationLayout', { main: 'pin'} ); 
  },
  name: 'pin'
});

FlowRouter.route( '/withdrawl', {
  action: function() {
    BlazeLayout.render( 'applicationLayout', { main: 'withdrawl'} ); 
  },
  name: 'withdrawl'
});

FlowRouter.route( '/processing', {
  action: function() {
    BlazeLayout.render( 'applicationLayout', { main: 'processing' } ); 
  },
  name: 'processing'
});

FlowRouter.route( '/success', {
  action: function() {
    BlazeLayout.render( 'applicationLayout', { main: 'success' } ); 
  },
  name: 'success'
});

