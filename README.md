------------------
Installation
------------------
1) Install Meteor - https://www.meteor.com/install  
2) Run "npm install", get dependencies  
3) Run command "meteor",  
4) View website at localhost:3000  


------------------
Technologies
------------------
Meteor - Javascript Development Platform designed for ease of use and rapid prototyping  
Blaze - Templating engine, similar to handlebars or php twig  
Flow Router - Routing library used to direct traffic to template views  

Why these technologies:   
-Rapid Prototyping - All the tools given to create prototypes and fast iterations  
-Ease of use, simplistic, similiar to coding styles of php frameworks.  
-Flexible - Meteor can be used with Angular or React, giving it flexibility for display.   
	But, Blaze is a lot like Twig of PHP, which I think is a lot more direct and simple than Angular or React.    
	Especially for newer javascript developers.   	


------------------
Notable Directories and Files
------------------

/client - holds all client side code, relevant to this project   
 	-main.html - parent template file
 	-/{page} - each page has it's own directory with relevant css, html, and javascript

routes.js - holds all routing code for url and one page functionality   

/models - not used, but models such as a Transaction Collection can be defined here    

/server- not used in the app, but ability to create MongoDB Collections on the server, and utilize other sever processing    


-----------------------------
Limitations
----------------------------
- Married to MongoDB. Cannot interface with any other database    
- Interfacing with external APIs not as straightforward as React or Angular
