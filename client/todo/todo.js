
import { Template } from 'meteor/templating';

import { Tasks } from '../../models/tasks.js';

import './todo.html';

 

Template.todo.helpers({

  tasks() {
    return Tasks.find({});
  },

});

Template.todo.events({
  'click .new-task'(event) {
    Tasks.insert({
    	"text":"new task!"
    });
  },
});

