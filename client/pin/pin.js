
import { Template } from 'meteor/templating';
import { Meteor } from 'meteor/meteor';
import './pin.html';


Template.pin.events({
  'submit .pin-form'(event) {
  	event.preventDefault();
  	var target = event.target;
    var pin = target.pin.value;
	 BlazeLayout.render( 'applicationLayout', { main: 'processing' } ); 
    if(pin != "1234") {
    	Meteor.setTimeout(function(){
	     BlazeLayout.render( 'applicationLayout', { main: 'pin',error:'Incorrect Pin'} ); 
	    },1500);
    	
    } else {
	    Meteor.setTimeout(function(){
	        FlowRouter.go('/withdrawl');   
	    },1500);
	}
  },
  'click .clear'(event) {
  	 $("#pin-form")[0].reset();
  	 $(".pin-input").focus();
  }
});

function reloadPin(callback) {
	 
	callback();
}


