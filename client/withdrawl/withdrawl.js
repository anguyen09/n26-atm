
import { Template } from 'meteor/templating';
import { Meteor } from 'meteor/meteor';
import './withdrawl.html';


Template.withdrawl.events({
  'click .withdrawl-amount'(event) {
    BlazeLayout.render( 'applicationLayout', { main: 'withdrawl-processing' } ); 
    Meteor.setTimeout(function(){
        FlowRouter.go('/success');   
    },2000);
  },

  'submit #custom-amount-form'(event) {
  	event.preventDefault();
    BlazeLayout.render( 'applicationLayout', { main: 'withdrawl-processing' } ); 
    Meteor.setTimeout(function(){
        FlowRouter.go('/success');   
    },2000);
  },
  'click .clear'(event) {
  	 $("#custom-amount-form")[0].reset();
  	 $("#custom-amount").focus();
  }
});

